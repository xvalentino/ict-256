$(function () {
    $("body").hide();
    $("body").fadeIn(2000);

    var page_original = true;

    $("img").on('click', function () {
        $("#content").css("background","grey");
        $("h1,h2").css("border-color", "red");
        $("p").css("color", "black");
        $("h2:not('.anecdote')").css("color", "red");
        $("h2.anecdote + p").css("color", "white");
        $("li").css("color", "black");
        page_original = false;
    });

});





