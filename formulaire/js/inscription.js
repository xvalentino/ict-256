$(function () {

    $.validator.addMethod("PWCHECK",
        function (value,element) {
            if(/^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)){
                return true;
            }else{
                return false;
            };
        }
    );

    $("#inscription_form").validate(
        {
            rules:{
                nom_per: {
                    required: true,
                    minlength: 2
                },
                prenom_per: {
                    required: true,
                    minlength: 2
                },
                email_per: {
                    required: true,
                    email: true
                },
                password_per: {
                    required: true,
                    minlength: 8,
                    PWCHECK: true
                },
                passwordConfirmation_per: {
                  required: true,
                  minlength: 8,
                  equalTo: "#password_per"
                }
            },
            messages:{
                nom_per: {
                    required: "Veuillez saisir votre nom",
                    minlength: "Votr nom doit être composé de deux caractères au minimum"
                },
                prenom_per: {
                    required: "Veuillez saisir votre prenom",
                    minlength: "Votr nom doit être composé de deux caractères au minimum"
                },
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre email doit avoir le format suivant : name@domain.com"
                },
                password_per: {
                    required: "Veuillez saisir votre mot de passe",
                    minlength: "Votre mot de passe est trop court",
                    PWCHECK: "Votre mot de passe doit contenir des: majuscules, minuscules, lettres, plus de 8 caractère, caractère spéciaux"
                },
                passwordConfirmation_per: {
                    equalTo: "Le mot de passe ne sont pas identique",
                    required: "Veuillez saisir une deuxième fois votre mot de passe"
                }
            }
        }
    )
});